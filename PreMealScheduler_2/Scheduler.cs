﻿using System;
using System.IO;
using System.Configuration;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using System.Data;
using System.Diagnostics;
using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.File;
namespace PreMealScheduler_2
{
    class Scheduler
    {
        public static StreamReader fileStreamReader;
        public static DataTable fileDataTable = new DataTable();
        public static string connstring = ConfigurationSettings.AppSettings["azureDBConnectionstring"].ToString();
        /// <summary>
        /// This is the entry point of the application
        /// </summary>
        /// <param name="args"></param>
        static void Main(string[] args)
        {
            LogProcess("-------------------------Scheduler #2 Begins @ " + DateTime.Now.ToString()+ "----------------------");
            StartProcess();
            LogProcess("-------------------------Scheduler #2 Ends @ " + DateTime.Now.ToString() + "-----------------------"+
                Environment.NewLine);
        }
        /// <summary>
        /// This is function where scheduler #2 process is happening
        /// </summary>
        public static void StartProcess()
        {
            try
            {
                //Process ONLY when file exist in the Azure File Storage Location
                if (CheckIfNewFileExist())
                {
                    LogProcess("input file named modpreselmeal.csv found");
                    //clean temp tables
                    ClearTempTables();
                    LogProcess("Temp Tables - tmp_premeal | tmp_selcustomer | tmp_selections | tmp_offer cleared");
                    //Read the input file
                    ReadFile();
                    LogProcess("File Data is Read and loaded into Data Table");
                    //Populate temp tables
                    PopulateTempTables(); 
                    //delete the input file                   
                    DeleteFile();                    
                    LogProcess("File is deleted from Azure storage location");
                }
                else
                {
                    LogProcess("No input file named modpreselmeal.csv NOT found in the azure file storage");
                }
            }
            catch (Exception e)
            {
                ClearTempTables();
                LogProcess("Exception while executing function " + new StackTrace(e).GetFrame(0).GetMethod().Name + 
                    "with the Error " + e.Message);
                LogError(e.Message, new StackTrace(e).GetFrame(0).GetMethod().Name);
            }
        }
        /// <summary>
        /// This function checks if new input file is found in azure file storage location
        /// </summary>
        /// <returns></returns>
        public static bool CheckIfNewFileExist()
        {
            try
            {
                // Parse the connection string for the storage account.
                CloudStorageAccount storageAccount = CloudStorageAccount.Parse(ConfigurationSettings.AppSettings["azurestorage"].ToString());

                // Create a CloudFileClient object for credentialed access to File storage.
                CloudFileClient fileClient = storageAccount.CreateCloudFileClient();

                // Get a reference to the file share we created previously.
                CloudFileShare share = fileClient.GetShareReference(ConfigurationSettings.AppSettings["azurefilesharename"].ToString());

                // Ensure that the share exists.
                if (share.Exists())
                {
                    CloudFileDirectory rootDirectory = share.GetRootDirectoryReference();
                    if (rootDirectory.Exists())
                    {
                        CloudFile file = rootDirectory.GetFileReference(ConfigurationSettings.AppSettings["azurefilename"].ToString());
                        if (file.Exists())
                        {
                            //upload file content to file stream reader
                            fileStreamReader = new StreamReader(file.OpenRead());
                            file = null;
                            rootDirectory = null;
                            share = null;
                            fileClient = null;
                            storageAccount = null;
                            return true;
                        }
                    }
                }
            }
            catch (Exception e)
            {
                throw e;
            }
            return false;
        }
        /// <summary>
        /// This function logs error in pm_log table in DlDBBot database
        /// </summary>
        /// <param name="error">error message to be logged in table</param>
        /// <param name="method">method in which error occurred.</param>
        public static void LogError(string error, string method)
        {
            var connection = GetSqlConnection(connstring);
            SqlCommand cmd = new SqlCommand("InsertLog", connection);
            try
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("@date", SqlDbType.VarChar).Value = DateTime.Now.ToString();
                cmd.Parameters.Add("@desc", SqlDbType.VarChar).Value = error;
                cmd.Parameters.Add("@method", SqlDbType.VarChar).Value = method;
                cmd.ExecuteNonQuery();
            }
            catch (Exception e)
            {
                throw e;
            }
            finally
            {
                cmd.Dispose();
                connection.Dispose();
            }
        }
        /// <summary>
        /// This function logs the process steps in log.txt file in azure file storage location.
        /// </summary>
        /// <param name="message">message to be logged</param>
        public static void LogProcess(string message)
        {
            try
            {
                // Parse the connection string for the storage account.
                CloudStorageAccount storageAccount = CloudStorageAccount.Parse(ConfigurationSettings.AppSettings["azurestorage"].ToString());

                // Create a CloudFileClient object for credentialed access to File storage.
                CloudFileClient fileClient = storageAccount.CreateCloudFileClient();

                // Get a reference to the file share we created previously.
                CloudFileShare share = fileClient.GetShareReference(ConfigurationSettings.AppSettings["azurefilesharename"].ToString());

                // Ensure that the share exists.
                if (share.Exists())
                {
                    CloudFileDirectory rootDirectory = share.GetRootDirectoryReference();
                    if (rootDirectory.Exists())
                    {
                        CloudFile file = rootDirectory.GetFileReference(ConfigurationSettings.AppSettings["LogFile"].ToString());
                        if (file.Exists())
                        {
                            StringBuilder sb = new StringBuilder();
                            sb.Append(file.DownloadText());
                            sb.Append(Environment.NewLine);
                            sb.Append(message);
                            file.UploadText(sb.ToString());
                            file = null;
                            rootDirectory = null;
                            share = null;
                            fileClient = null;
                            storageAccount = null;
                        }
                    }
                }
            }
            catch (Exception e)
            {
                throw e;
            }
        }
        /// <summary>
        /// This function gets the SQL Connection for furture use
        /// </summary>
        /// <param name="conn">DB Connection string</param>
        /// <returns>SQL Connection object</returns>
        private static SqlConnection GetSqlConnection(string conn)
        {
            try
            {
                var connection = new SqlConnection(conn);
                connection.Open();
                return connection;
            }
            catch (Exception e)
            {
                ClearTempTables();
                throw e;
            }
        }
        /// <summary>
        /// This function is used to clear temp tables
        /// </summary>
        public static void ClearTempTables()
        {
            var connection = GetSqlConnection(connstring);
            SqlCommand cmd = new SqlCommand("CleanTempTables", connection);
            try
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.ExecuteNonQuery();
            }
            catch (Exception e)
            {
                throw e;
            }
            finally
            {
                connection.Dispose();
                cmd.Dispose();
            }
        }
        /// <summary>
        /// This function is used to read the file and load the datatable
        /// </summary>
        public static void ReadFile()
        {
            try
            {
                string[] columnNames = null;
                //  Get the first line
                string line = fileStreamReader.ReadLine();
                columnNames = line.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries);
                //  Add the columns to the data table
                foreach (string colName in columnNames)
                    fileDataTable.Columns.Add(colName);
                //  Read the file
                string[] columns;
                while ((line = fileStreamReader.ReadLine()) != null)
                {
                    columns = line.Replace(" ", "").Split(new string[] { "," }, StringSplitOptions.None);
                    fileDataTable.Rows.Add(columns);
                }
                columnNames = columns = null;
                line = null;
            }
            catch (Exception e)
            {
                throw e;
            }
        }
        /// <summary>
        /// This function is used to populate the temp tables from the data table loaded
        /// </summary>
        public static void PopulateTempTables()
        {
            var connection = GetSqlConnection(connstring);
            SqlCommand cmd = new SqlCommand("PopulateTmpPremealDD", connection);
            try
            {
                SqlBulkCopy bulkcopy = new SqlBulkCopy(connstring);
                bulkcopy.DestinationTableName = ConfigurationSettings.AppSettings["MainTable"].ToString();
                bulkcopy.WriteToServer(fileDataTable);
                LogProcess("Data from Data table is loaded into TMP_PREMEAL Table");
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.ExecuteNonQuery();
                LogProcess("Data from Data table is loaded into PM_PREMEAL Table");
                bulkcopy.Close();
            }
            catch (Exception e)
            {
                throw e;
            }
            finally
            {
                connection.Dispose();
                cmd.Dispose();
            }
        }
        /// <summary>
        /// Delete the input file from the azure file storage location once processing done.
        /// </summary>
        public static void DeleteFile()
        {
            try
            {
                // Parse the connection string for the storage account.
                CloudStorageAccount storageAccount = CloudStorageAccount.Parse(ConfigurationSettings.AppSettings["azurestorage"].ToString());

                // Create a CloudFileClient object for credentialed access to File storage.
                CloudFileClient fileClient = storageAccount.CreateCloudFileClient();

                // Get a reference to the file share we created previously.
                CloudFileShare share = fileClient.GetShareReference(ConfigurationSettings.AppSettings["azurefilesharename"].ToString());

                // Ensure that the share exists.
                if (share.Exists())
                {
                    CloudFileDirectory rootDirectory = share.GetRootDirectoryReference();
                    if (rootDirectory.Exists())
                    {
                        CloudFile file = rootDirectory.GetFileReference(ConfigurationSettings.AppSettings["azurefilename"].ToString());
                        if (file.Exists())
                        {
                            file.Delete();
                            file = null;
                            share = null;
                            fileClient = null;
                            storageAccount = null;
                        }
                    }
                }
            }
            catch (Exception e)
            {
                throw e;
            }
        }
    }
}
